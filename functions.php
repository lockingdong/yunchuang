<?php
/**
 * ken-cens.com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ken-cens.com
 */


/**
 * Class WP_Combine_Queries
 * 
 * @uses WP_Query_Empty
 * @link https://stackoverflow.com/a/23704088/2078474
 *
 */

class WP_Combine_Queries extends WP_Query 
{
    protected $args    = array();
    protected $sub_sql = array();
    protected $sql     = '';

    public function __construct( $args = array() )
    {
        $defaults = array(
            'sublimit'       => 1000,
            'posts_per_page' => 10,
            'paged'          => 1,
            'args'           => array(),
        );

        $this->args = wp_parse_args( $args, $defaults );

        add_filter( 'posts_request',  array( $this, 'posts_request' ), PHP_INT_MAX  );

        parent::__construct( array( 'post_type' => 'post' ) );
    }

    public function posts_request( $request )
    {
        remove_filter( current_filter(), array( $this, __FUNCTION__ ), PHP_INT_MAX  );

        // Collect the generated SQL for each sub-query:
        foreach( (array) $this->args['args'] as $a )
        {
            $q = new WP_Query_Empty( $a, $this->args['sublimit'] );
            $this->sub_sql[] = $q->get_sql();
            unset( $q );
        }

        // Combine all the sub-queries into a single SQL query.
        // We must have at least two subqueries:
        if ( count( $this->sub_sql ) > 1 )
        {
            $s = '(' . join( ') UNION (', $this->sub_sql ) . ' ) ';

            $request = sprintf( "SELECT SQL_CALC_FOUND_ROWS * FROM ( $s ) as combined LIMIT %s,%s",
                $this->args['posts_per_page'] * ( $this->args['paged']-1 ),
                $this->args['posts_per_page']
            );          
        }
        return $request;
    }

} // end class

/**
 * Class WP_Query_Empty
 *
 * @link https://stackoverflow.com/a/23704088/2078474
 */

class WP_Query_Empty extends WP_Query 
{
    protected $args      = array();
    protected $sql       = '';
    protected $limits    = '';
    protected $sublimit  = 0;

    public function __construct( $args = array(), $sublimit = 1000 )
    {
        $this->args     = $args;
        $this->sublimit = $sublimit;

        add_filter( 'posts_clauses',  array( $this, 'posts_clauses' ), PHP_INT_MAX  );
        add_filter( 'posts_request',  array( $this, 'posts_request' ), PHP_INT_MAX  );

        parent::__construct( $args );
    }

    public function posts_request( $request )
    {
        remove_filter( current_filter(), array( $this, __FUNCTION__ ), PHP_INT_MAX );
        $this->sql = $this->modify( $request );             
        return '';
    }

    public function posts_clauses( $clauses )
    {
        remove_filter( current_filter(), array( $this, __FUNCTION__ ), PHP_INT_MAX  );
        $this->limits = $clauses['limits'];
        return $clauses;
    }

    protected function modify( $request )
    {
		$request = str_ireplace( 'SQL_CALC_FOUND_ROWS', '', $request );
		

        if( $this->sublimit > 0 )
            return str_ireplace( $this->limits, sprintf( 'LIMIT %d', $this->sublimit ), $request );
        else
            return $request;
    }

   public function get_sql( )
    {
        return $this->sql;
    }

} // end class

if ( ! function_exists( 'ken_cens_com_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ken_cens_com_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ken-cens.com, use a find and replace
	 * to change 'ken-cens-com' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ken-cens-com', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'ken-cens-com' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ken_cens_com_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ken_cens_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ken_cens_com_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ken_cens_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'ken_cens_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ken_cens_com_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ken-cens-com' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ken-cens-com' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ken_cens_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ken_cens_com_scripts() {
	wp_enqueue_style( 'ken-cens-com-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ken-cens-com-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'ken-cens-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ken_cens_com_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


function my_pagination() {

	global $the_query;
	
	if ( $the_query->max_num_pages <= 1 ) return; 
	
	$big = 999999999; // need an unlikely integer
	
	$pages = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $the_query->max_num_pages,
		'type'  => 'array',
		'prev_text' => "↼",
		'next_text' => "⇀",
	) );
	if( is_array( $pages ) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
		echo '<ul class="pagination justify-content-center">';
		foreach ( $pages as $page ) {
				echo "<li class='page-item'>$page</li>";
		}
	   echo '</ul>';
	}
}






function dong_create_video(){
	register_post_type('video',
		array(
			'labels' => array(
				'name' => '影音專區',
				'singular_name' => '影音專區',
			),
			'public' => true,
			'menu_position' => 7,
			'has_archive' => false,
			'rewrite' => true ,
			'show_in_rest' => true,
			'has_archive' => true,
			'rest_base' => 'video',
			//'taxonomies' => array('category'), //增加分類
			'rest_controller_class' => 'WP_REST_Posts_Controller',
			'supports' => array(
				'author',
				'title',
				'editor',
				//'excerpt',
				//'thumbnail',
				//'comments',
				
			),
		)
	);

}
add_action('init', 'dong_create_video', 1);


function dong_create_taxonomies(){
	$labels = array(
		'name' => '影片分類',
		'singular_name' => '影片分類',
	);

	register_taxonomy('video-type', array('video'),
		array(
			'has_archive' => true,
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
		)
	);
}
add_action('init', 'dong_create_taxonomies', 1);



add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});




// add_filter('acf/fields/relationship/query/key=field_5bc4682de1151', 'my_relationship_query', 10, 3);
// function my_relationship_query($args, $field, $post) {
//     // show only pages with this special template
//     $args['meta_query'] = array(
//         'relation' => 'AND',
//         array(
//             'key' => '_wp_page_template',
//             'value' => 'page-service.php',
//             'compare' => '='
//         )
//     );
//     return $args;
// }
add_filter('acf/fields/page_link/query/key=field_5bc4565792d5e', 'my_relationship_query', 10, 3);
function my_relationship_query( $args, $field, $post )
{
	// show only pages with this special template
	$args['meta_key'] = '_wp_page_template';
	$args['meta_value'] = 'page-service.php';
	return $args;
}

// add_filter('acf/fields/relationship/query/key=field_5bc4682de1151', 'my_relationship_query', 10, 3);
// function my_relationship_query( $args, $field, $post )
// {
// 	// show only pages with this special template
// 	$args['meta_key'] = '_wp_page_template';
// 	$args['meta_value'] = 'page-service.php';
// 	return $args;
// }

function wpdocs_scripts_method() {
    wp_enqueue_script( 'custom-script', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method' );



// function wpse196289_default_page_template() {
//     global $post;
//     if ( 'page' == $post->post_type 
//         && 0 != count( get_page_templates( $post ) ) 
//         && get_option( 'page_for_posts' ) != $post->ID // Not the page for listing posts
//         && '' == $post->page_template // Only when page_template is not set
//     ) {
//         $post->page_template = "page-service.php";
//     }
// }
// add_action('add_meta_boxes', 'wpse196289_default_page_template', 1);


function wpb_remove_screen_options() { 
	if(!current_user_can('manage_options')) {
	return false;
	}
	return true; 
	}
add_filter('screen_options_show_screen', 'wpb_remove_screen_options');



function disable_new_posts() {
	// Hide sidebar link
	// global $submenu;
	//unset($submenu['edit.php?post_type=jxta_home'][10]);
	// Hide link on listing page
	
	
		if (isset($_GET['page']) && $_GET['page'] == 'wpcf7') {
			echo '<style type="text/css">
			.add-new-h2,#form-panel,#ui-id-1{ display:none !important; }
			</style>';
		}
	
	
}




$user = wp_get_current_user();
$allowed_roles = array('editor', 'author');

if( array_intersect($allowed_roles, $user->roles ) ) {
	add_action('admin_menu', 'disable_new_posts');
}




function disable_new_page() {
	// Hide sidebar link
	// global $submenu;
	//unset($submenu['edit.php?post_type=jxta_home'][10]);
	
	// Hide link on listing page
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
		echo '<style type="text/css">
		.page-title-action { display:none; }
		</style>';
	 }
	}
	add_action('admin_menu', 'disable_new_page');



//display: none //page quick edit
function remove_page_quick_edit() {
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
	    echo '<style type="text/css">
			.editinline { display:none; }
	    </style>';
	}
}
add_action('admin_menu', 'remove_page_quick_edit');




function remove_sticky_post() {
	echo '<style type="text/css">
		input[name=sticky] { display:none !important; }
		input[name=sticky] + span { display:none !important; }
	</style>';
}
add_action('admin_footer', 'remove_sticky_post', 9);


// remove toolbar items
// https://digwp.com/2016/06/remove-toolbar-items/
function shapeSpace_remove_toolbar_node($wp_admin_bar) {
	
	// replace 'updraft_admin_node' with your node id
	$wp_admin_bar->remove_node('new-content');
	$wp_admin_bar->remove_node('customize');
	
}
add_action('admin_bar_menu', 'shapeSpace_remove_toolbar_node', 999);



add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'video'; // change to your post type
	$taxonomy  = 'video-type'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}


add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'video'; // change to your post type
	$taxonomy  = 'video-type'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}





// add_action( 'admin_footer', 'my_action' );
// add_action( 'wp_ajax_nopriv_my_action', 'my_action' ); // This lines it's because we are using AJAX on the FrontEnd.

// function my_action(){
//     // $fieldname = $_POST['fieldname']; // This variable will get the POST 'fieldname'
//     $fieldvalue = $_POST['value'];  // This variable will get the POST 'fieldvalue'
//     // $postid = $_POST['postid'];             // This variable will get the POST 'postid'
//     // update_post_meta($postid, $fieldname, $fieldvalue); // We will update the field.
// 	// wp_die($fieldname = '', $fieldvalue = '', $postid = ''); // this is required to terminate immediately and return a proper response
	

// 	update_field('field_5bd976ca7199b', $fieldvalue, 300);

// } 



function test_function() {
	// Set variables
	$input_test = $_POST['test'];
	$post_id = $_POST['post_id'];
	// Check variables for fallbacks
	if (!isset($input_test) || $input_test == "") { $input_test = "Fall Back"; }
	// Update the field
	update_field('field_5bda8c042d8f1', $input_test, $post_id);
}
add_action( 'wp_ajax_nopriv_test_function',  'test_function' );
add_action( 'wp_ajax_test_function','test_function' );







function sticky_post_click() {
	echo '<script>jQuery(".to_top").change(
		function(){
			var checked;
			var id = jQuery(this).parents("tr").attr("id").substring(5);
			if (jQuery(this).is(":checked")) {
				checked=1;
			}else{
				checked=0;
			}
			console.log(checked);
			console.log(jQuery(this).parents("tr").attr("id").substring(5));
			jQuery.ajax({
				url: "https://ycdesign.tw/wp-admin/admin-ajax.php?action=test_function",
				type: "post",
				data: {
					"post_id":id,
					"test": checked,
				},
				success: function(data) {
					console.log(data);
				},
				error: function(data) {
					console.log("FAILURE");
				}
			});
		});</script>';
}
add_action('admin_footer', 'sticky_post_click', 10,1);





// Add the custom columns to the book post type:
add_filter( 'manage_post_posts_columns', 'act_status_columns' );
function act_status_columns($columns) {
    //unset( $columns['author'] );
    $columns['on_top'] = __( '置頂', 'your_text_domain' );
    //$columns['publisher'] = __( 'Publisher', 'your_text_domain' );
	// $newColumns = array();
	// $newColumns['status'] = '活動狀態';
    return $columns;
}


// Add the data to the custom columns for the book post type:
add_action( 'manage_post_posts_custom_column' , 'custom_act_column', 10, 2 );
function custom_act_column( $column, $post_id ) {
	switch ( $column ) {
		case 'on_top' :
			$bool =  get_post_meta( $post_id , 'test' , true ); 
			$checked = $bool==1?'checked':'';
			//if($checked==1)
			//echo (!comments_open())?"已結束":"<span style='color:red;'>開放中</span>";
			echo '<input class="to_top" type="checkbox"'. $checked .'>';
			break;

	}
}


add_filter( 'manage_edit-post_sortable_columns', 'my_website_manage_sortable_columns' );
function my_website_manage_sortable_columns( $sortable_columns ) {

	$sortable_columns[ 'on_top' ] = "on_top";
	// Let's also make the film rating column sortable
	//$sortable_columns[ 'film_rating_column' ] = 'film_rating';
	return $sortable_columns;

}
add_action( 'pre_get_posts', 'mycpt_custom_orderby' );

function mycpt_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'on_top' == $orderby ) {
    $query->set( 'meta_key', 'test' );
    $query->set( 'orderby', 'meta_value' );
  }
}




/////video sticky post//////




function video_update() {
	// Set variables
	$input_test = $_POST['test'];
	$post_id = $_POST['post_id'];
	// Check variables for fallbacks
	if (!isset($input_test) || $input_test == "") { $input_test = "Fall Back"; }
	// Update the field
	update_field('field_5bda9ff431ba5', $input_test, $post_id);
}
add_action( 'wp_ajax_nopriv_video_update',  'video_update' );
add_action( 'wp_ajax_video_update','video_update' );







function video_sticky_post_click() {
	echo '<script>jQuery(".video_to_top").change(
		function(){
			var checked;
			var id = jQuery(this).parents("tr").attr("id").substring(5);
			if (jQuery(this).is(":checked")) {
				checked=1;
			}else{
				checked=0;
			}
			console.log(checked);
			console.log(jQuery(this).parents("tr").attr("id").substring(5));
			jQuery.ajax({
				url: "https://ycdesign.tw/wp-admin/admin-ajax.php?action=video_update",
				type: "post",
				data: {
					"post_id":id,
					"test": checked,
				},
				success: function(data) {
					console.log(data);
				},
				error: function(data) {
					console.log("FAILURE");
				}
			});
		});</script>';
}
add_action('admin_footer', 'video_sticky_post_click', 10,2);





// Add the custom columns to the book post type:
add_filter( 'manage_video_posts_columns', 'video_status_columns' );
function video_status_columns($columns) {
    //unset( $columns['author'] );
    $columns['video_on_top'] = __( '置頂', 'your_text_domain' );
    //$columns['publisher'] = __( 'Publisher', 'your_text_domain' );
	// $newColumns = array();
	// $newColumns['status'] = '活動狀態';
    return $columns;
}


// Add the data to the custom columns for the book post type:
add_action( 'manage_video_posts_custom_column' , 'custom_video_column', 10, 2 );
function custom_video_column( $column, $post_id ) {
	switch ( $column ) {
		case 'video_on_top' :
			$bool =  get_post_meta( $post_id , 'test2' , true ); 
			$checked = $bool==1?'checked':'';
			//if($checked==1)
			//echo (!comments_open())?"已結束":"<span style='color:red;'>開放中</span>";
			echo '<input class="video_to_top" type="checkbox"'. $checked .'>';
			//echo $checked;
			break;

	}
}


add_filter( 'manage_edit-video_sortable_columns', 'my_website_manage_sortable_columns2' );
function my_website_manage_sortable_columns2( $sortable_columns ) {

	$sortable_columns[ 'video_on_top' ] = "video_on_top";
	// Let's also make the film rating column sortable
	//$sortable_columns[ 'film_rating_column' ] = 'film_rating';
	return $sortable_columns;

}



add_action( 'pre_get_posts', 'video_custom_orderby' );

function video_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');

  if ( 'video_on_top' == $orderby ) {
    $query->set( 'meta_key', 'test2' );
    $query->set( 'orderby', 'meta_value_num' );
  }
}




flush_rewrite_rules(); 