<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */

$al_cat_slug = get_queried_object()->slug;
$al_cat_name = get_queried_object()->name;
$middle_pages = json_decode('[
    {
		"name": "影音專區",
		"url": "'. get_the_permalink(240) .'"
	}
]');
$page_title = $al_cat_name;
$custom_page_title = get_field('page_title', 240);
$cover_bg = get_field('cover_img', 240)['url'];
get_header(); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>



<?php
    $cur = get_query_var('paged');

    $the_query = new WP_Query(array(
        'post_type' => 'video', // Your Post type Name that You Registered
        'posts_per_page' => 9,
        'order' => 'DSC',
        'paged' => $cur,
        'meta_key'			=> 'test2',
		    'orderby'			=> 'meta_value',
        'tax_query' => array(
            array(
                'taxonomy' => 'video-type',
                'field' => 'slug',
                'terms' => $al_cat_slug
            )
        )
    ));

?>

<section class="container content-wrapper video-list">
  <div class="items-tab">

	<?php
		$terms = get_terms('video-type');

		foreach ( $terms as $term ) {
		$term_link = get_term_link( $term );

		// If there was an error, continue to the next term.
		if ( is_wp_error( $term_link )) {
			continue;
		}
		?>
		<a class="btn btn-tab <?php echo (($term->slug===$al_cat_slug)?"btn-active":"");?>" href="<?php echo $term_link; ?>">
            <?php echo $term->name; ?>
            
		</a>
		<?php
		}
	?>
  </div>
  <div class="row">

<?php while ($the_query -> have_posts()) : 
    $the_query -> the_post(); 
?>

    <div class="col-lg-4 col-sm-6">
      <div class="card-item video-item-card">
        <a href="<?php echo get_permalink() ;?>">
          <div class="card-img">
		  	<img src="<?php echo get_field('cover')['url']; ?>" 
				alt="<?php echo get_field('cover')['alt']; ?>"
			>  
            <div class="player-btn-wrapper">
                <div class="player-btn"></div>
            </div>
          </div>
        </a>
        <div class="card-body">
          <h5 class="card-title"><?php echo get_the_title(); ?></h5>
          <div class="card-info">
            <span><?php echo get_the_content(); ?></span>
          </div>
        </div>
      </div>
	</div>
	

<?php
    endwhile;
    wp_reset_postdata(); 
?>





    
  </div>
  <!-- 分頁 -->
  <?php my_pagination(); ?>
</section>




<?php
get_footer();