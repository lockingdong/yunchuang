<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */
$al_cat_slug = get_the_terms( $post->ID , 'video-type' )[0]->slug;
$al_cat_name = get_the_terms( $post->ID , 'video-type' )[0]->name;
$middle_pages = json_decode('[
    {
		"name": "影音專區",
		"url": "'. get_the_permalink(240) .'"
	},
	{
		"name": "'. $al_cat_name .'",
		"url": "'. get_home_url() .'/video-type/'.$al_cat_slug.'"
	}
]');
$page_title = get_the_title();
$custom_page_title = get_field('page_title', 240);
$cover_bg = get_field('cover_inner_img', 240)['url'];
get_header(); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<section class="container-fluid video-page">
  <div class="container video-content-wrapper">
    <h3><?php echo get_the_title(); ?></h3>
		<?php if(get_field('youtube')): ?>
    <div class="video-content-area">
		
			<?php the_field('youtube'); ?>
		
    </div>
		<?php endif; ?>
		<?php //if(get_field("3d_url")): ?>
			<!-- <div class="threed-wrap">
				<iframe allowfullscreen="true" src="<?php //echo get_field("3d_url"); ?>"></iframe>
			</div> -->
		<?php //endif; ?>


		<?php if(get_field("img")): ?>
		<div class="video-content-area">
		<?php endif; ?>


		<?php if(get_field("url")): ?>
			<a href="<?php echo get_field("url");?>" target="_blank">
		<?php endif ?>

		<?php if(get_field("img")): ?>
		<img 
				src="<?php echo get_field("img")["url"]; ?>" 
				alt="<?php echo get_field("img")["alt"]; ?>">
		<?php endif; ?>

		<?php if(get_field("url")): ?>
			</a>
		<?php endif ?>

		<?php if(get_field("img")): ?>
		</div>
		<?php endif; ?>

		<?php if(get_field("img")): ?>
		<p>⇧ 點上圖前往查看3D環景效果</p>
		<?php endif; ?>
    <div class="video-content-info">
		
		<?php the_content(); ?>
    </div>
  </div> 
</section>







<?php endwhile; ?>
<?php endif; ?>


	

<?php
//get_sidebar();
get_footer();
