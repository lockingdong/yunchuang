<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="profile" href="http://gmpg.org/xfn/11"> -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="src/dist/images/favicon-yunchang.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/css/all.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/images/favicon-yunchang.ico">
    <meta property="og:url"           content="<?php echo get_permalink() ;?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?php echo get_the_title(); ?>" />
    <meta property="og:description"   content="<?php echo get_the_title(); ?>-云創設計||YUNCHUANG" />
    
    
    <?php if(get_field("slick")[0]["image"]["url"]): ?>
    <meta property="og:image"         content="<?php echo get_field("slick")[0]["image"]["url"]?>" />
    <?php endif; ?>

    <?php if(get_field("banner")[0]["bg_img"]["url"]): ?>
    <meta property="og:image"         content="<?php echo get_field("banner")[0]["bg_img"]["url"]?>" />
    <?php endif; ?>


    <meta property="og:image:alt"     content="施工圖設計、3D空間圖、360度環景圖、櫃體設計、監工服務" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="container-fluid navbar navbar-bg" >
  <nav class="navbar  navbar-expand-lg navbar-light ">
    <a class="navbar-brand" href="<?php echo get_the_permalink(179); ?>" >
      <img src="<?php echo get_field('site_logo', 179)['url']; ?>" 
        alt="<?php echo get_field('site_logo', 179)['alt']; ?>"
      >
      <!-- <img src="<?php //echo bloginfo('stylesheet_directory'); ?>/src/dist/images/logo-top.svg" alt="YUNCHUANG logo"> -->
    </a>
    <button class="navbar-toggler" type="button" id="dropdownbutton">
      <div class="hamburger" id="hamburger-9">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </div>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="<?php echo get_the_permalink(6); ?>"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" itemprop="startDate" content="2015-09-07">
            <p class="nav-link-tw">公司介紹</p>
            <p class="nav-link-en">ABOUT US</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item dropdowm-link-a" data-target="about-area" href="<?php echo get_the_permalink(6); ?>?#about" >關於云創</a>
            <a class="dropdown-item dropdowm-link-a" data-target="workflow-area" href="<?php echo get_the_permalink(6); ?>?#workflow">作業流程</a>
            <a class="dropdown-item dropdowm-link-a" data-target="charge-area" href="<?php echo get_the_permalink(6); ?>?#charge-reference" >收費參考</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">服務項目</p>
            <p class="nav-link-en">SERVICES</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="<?php echo get_the_permalink(82); ?>">室內設計</a>
            <a class="dropdown-item" href="<?php echo get_the_permalink(199); ?>">裝修工程</a>
            <a class="dropdown-item" href="<?php echo get_the_permalink(266); ?>">3D 設計</a>
            <a class="dropdown-item" href="<?php echo get_the_permalink(269); ?>">系統工程</a>
            <?php //if( have_rows('service_url', 179) ): ?>
                <?php //while( have_rows('service_url', 179)): the_row(); ?>

                    <?php 
                        //$post_id = get_sub_field('page_url', false, false);
                    ?>
                    


                    <?php //if( $post_id ): ?>
                        <!-- <a class="dropdown-item" href="<?php //echo get_the_permalink($post_id); ?>"><?php //echo get_the_title($post_id); ?></a> -->
                    <?php //endif; ?>


                <?php //endwhile; ?>
            <?php //endif; ?>
          </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="<?php echo get_category_link('4'); ?>"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">作品欣賞</p>
            <p class="nav-link-en">PROJECTS</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <!-- <a class="dropdown-item" href="project-list.php">住宅空間</a>
            <a class="dropdown-item" href="project-list.php">商業空間</a>
            <a class="dropdown-item" href="project-list.php">3D 作品</a>
            <a class="dropdown-item" href="project-list.php">系統案例</a> -->
            <?php 

            $categories = get_categories();
                foreach($categories as $category) {
                echo '<a class="dropdown-item" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
            }

            ?>
          </div>
        </li>
        <li class="nav-item dropdown">
          <?php $terms = get_terms('video-type'); ?>

          <a class="nav-link dropdown-toggle" href="<?php echo get_term_link( $terms[0] ); ?>"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">影音專區</p>
            <p class="nav-link-en">MEDIA</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <!-- <a class="dropdown-item" href="video-list.php">工程專輯</a>
            <a class="dropdown-item" href="video-list.php">3D 環景</a>
            <a class="dropdown-item" href="video-list.php">影音教學</a> -->
            <?php
              

              foreach ( $terms as $term ) {
              $term_link = get_term_link( $term );

              // If there was an error, continue to the next term.
              if ( is_wp_error( $term_link )) {
                continue;
              }
              ?>
              <a class="dropdown-item" href="<?php echo $term_link; ?>">
                      <?php echo $term->name; ?>
                      
              </a>
              <?php
              }
            ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo get_the_permalink(172); ?>" >
            <p class="nav-link-tw">聯絡我們</p>
            <p class="nav-link-en">CONTACT US</p>
          </a>
        </li>
      </ul>
    </div>
  </nav>
</header>

