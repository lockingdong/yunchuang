<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ken-cens.com
 */

?>

<section class="container-fluid contact-info-section"  >
  <div class="container contact-info">
    <img src="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/images/logo-footer.svg"  class="contact-logo" alt="logo">
    <p itemprop="addressLocality"><?php echo get_field('address1', 179) ?></p>
    <p itemprop="addressLocality"><?php echo get_field('address2', 179) ?></p>
    <p itemprop="addressLocality"><?php echo get_field('tel1', 179) ?></p>
    <p itemprop="addressLocality"><?php echo get_field('tel2', 179) ?></p>
    <p>
      <a href="<?php echo get_field('youtube_url', 179) ?>" class="social-icon"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/images/youtube.svg" alt="youtube"></a>
      <a href="<?php echo get_field('fb_url', 179) ?>" class="social-icon"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/images/facebook.svg" width="20px" alt="facebook"><span>follow us on Facebook</span></a> 
    </p>
    <div class="qrcode-line">
      <p class="mb-0">歡迎諮詢</p>
      <!-- <img src="<?php //echo bloginfo('stylesheet_directory'); ?>/src/dist/images/QRcode-test.png" alt="line-QRcode"> -->
	  <img src="<?php echo get_field('line_qr', 179)['url']; ?>" 
		   alt="<?php echo get_field('line_qr', 179)['alt']; ?>"
		>
    </div> 
    <div class="gotop">
        ↾
    </div>
  </div>
</section>
<footer class="container-fluid"> 
  <p itemprop="startDate" content="2015-09-07"><?php echo get_field('copyright', 179) ?> <span>All right reserved</span></p>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script src="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/js/all.min.js"></script>

<?php wp_footer(); ?>

</body>
</html>
