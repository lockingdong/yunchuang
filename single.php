<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ken-cens.com
 */

$categories = get_the_category();
$middle_pages = json_decode('[
	{
		"name": "作品欣賞",
		"url": "'. get_the_permalink(148) .'"
	},
	{
		"name": "'. $categories[0]->name .'",
		"url": "'. get_home_url() . '/category/'. $categories[0]->slug .'"
	}
]');

$page_title = get_the_title();
$custom_page_title = get_field('page_title', 148);
$cover_bg = get_field('cover_inner_img', 148)['url'];
get_header(); ?>
<script>
	(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


</script>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>

<style type="text/css">
	.project-content-title {
	overflow: hidden;
	}
	.project-main-slider {
	  margin-bottom: 20px;
	}
	.project-main-slider .slick-next, .project-main-slider .slick-prev {
	  position: absolute;
	  top: 50%;
	  transform: translateY(-50%);
	  z-index: 99;
	  color: #000;
	  background: transparent;
	  border: 0;
	  cursor: pointer;
	  font-size: 36px;
	  transition: color .3s;
	}
	.project-main-slider .slick-next:hover, .project-main-slider .slick-prev:hover {
	  color: #fcc802;
	}
	.project-main-slider .slick-prev {
	  left: -5%;
	}
	.project-main-slider .slick-next {
	  right: -5%;
	}
	.project-main-slider .fas{
	  font-family: "Font Awesome 5 Free"!important;
	}

	@media (max-width: 576px) {
	  .project-main-slider .slick-prev {
	    display: none
	  }

	  .project-main-slider .slick-next {
	    display: none
	  }
	}

</style>



<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<section class="container content-wrapper">
  <!-- 作品主輪播 -->
	<h2 class="project-content-title before-line"><?php echo get_the_title(); ?></h2>
	



   <div class="project-main-slider">

   <?php if( have_rows('slick') ): ?>
		<?php while( have_rows('slick') ): the_row(); ?>

		  <div class="project-main-slider-item">
			<img src="<?php echo get_sub_field('image')['url']; ?>" 
				alt="<?php echo get_sub_field('image')['alt']; ?>"
			>
		  </div>
		<?php endwhile; ?>
	<?php endif; ?>
	
   </div>
   <div class="project-sub-slider">
    
<?php if( have_rows('slick') ): ?>
    <?php while( have_rows('slick') ): the_row(); ?>

        <div class="project-sub-slider-item">
        <img 
						 style="cursor:pointer;"
						 src="<?php echo get_sub_field('image')['url']; ?>" 
             alt="<?php echo get_sub_field('image')['alt']; ?>"
		>
		</div>

    <?php endwhile; ?>
<?php endif; ?>

    
  </div>
  <!-- 主持設計師 -->
  <div class="project-designer">

	<img src="<?php echo get_field('artist_photo')['url']; ?>" 
		alt="<?php echo get_field('artist_photo')['alt']; ?>"
	>
    <div class="project-desinger-info">
      <p><?php echo get_field('artist_job'); ?></p>
      <h3><?php echo get_field('artist_name'); ?></h3>
    </div>
  </div>
  <!-- 空間資訊 -->
  <div class="row space-info mb-0">
    <div class="col-md-6 space-info-col">
		<span class="space-info-title"><?php echo get_field('text1'); ?></span>
		<span><?php echo get_field('content1'); ?></span>
	</div>
    <div class="col-md-6 space-info-col">
		<span class="space-info-title"><?php echo get_field('text2'); ?></span>
		<span><?php echo get_field('content2'); ?></div>
    <div class="col-lg-6 space-info-col">
		<span class="space-info-title"><?php echo get_field('text3'); ?></span>
		<span><?php echo get_field('content3'); ?>坪</span>
	</div>
    <div class="col-lg-6 space-info-col">
		<span class="space-info-title"><?php echo get_field('text4'); ?></span>
		<span><?php echo get_field('content4'); ?></span>
	</div>
    <div class="col-12 space-info-col">
		<span class="space-info-title"><?php echo get_field('text5'); ?></span>
		<span><?php echo get_field('content5'); ?></span>
	</div>
    <div class="col-12 space-fb-share">
      分享至 
      <div id="fb-share-button">
        <svg viewBox="0 0 12 12" preserveAspectRatio="xMidYMid meet">
          <path class="svg-icon-path" d="M9.1,0.1V2H8C7.6,2,7.3,2.1,7.1,2.3C7,2.4,6.9,2.7,6.9,3v1.4H9L8.8,6.5H6.9V12H4.7V6.5H2.9V4.4h1.8V2.8 c0-0.9,0.3-1.6,0.7-2.1C6,0.2,6.6,0,7.5,0C8.2,0,8.7,0,9.1,0.1z"></path>
        </svg>
	  </div>
	  <div class="fb-like" 
			data-href="<?php get_the_permalink($post->ID); ?>" 
			data-layout="button" 
			data-action="like" 
			data-size="large" 
			data-show-faces="false" data-share="false"
			style="vertical-align:middle;"
			>
			
	  </div>
    </div>
  </div>
   <!-- 上下頁 -->
   <div class="project-upper-lower">
<?php 
	$prev_post = get_adjacent_post(true, '', true);
	if(!empty($prev_post)):
?>
	<a class="project-lower" href="<?php echo get_permalink($prev_post->ID) ;?>">
		<span>↼</span>
		<div>
			<p class="upper-lower-title"><?php echo $prev_post->post_title ;?></p>  
			<p>
				<?php echo get_field("content2", $prev_post->ID); ?>,
				<?php echo get_field("content3", $prev_post->ID); ?>坪
			</p>
		</div>
	</a>
	
<?php else: ?>
	<p>沒有上一篇了！</p>
<?php endif; ?>
<?php 
	$next_post = get_adjacent_post(true, '', false);
	if(!empty($next_post)):
?>
	<a class="project-upper" href="<?php echo get_permalink($next_post->ID) ;?>">
		<div>
			<p class="upper-lower-title"><?php echo $next_post->post_title ;?></p>  
			<p>
				<?php echo get_field("content2", $next_post->ID); ?>,
				<?php echo get_field("content3", $next_post->ID); ?>坪
			</p>
		</div>
		<span>⇀</span>
	</a>
<?php else: ?>
	<p>沒有下一篇了！</p>
<?php endif; ?>
</div>

  <!-- 精選介紹 -->



<?php 
	$related_posts = get_field("pick");
?>
  <h2 class="project-content-title after-line">精選介紹</h2>
  <div class="project-introduction-slider">
	



	<?php if( $related_posts ): ?>
	<?php foreach( $related_posts as $post): // variable must be called $post (IMPORTANT) ?>
	<?php setup_postdata($post); ?>

    <div class="card-item">
      <a href="<?php the_permalink(); ?>">
        <div class="card-img">
		  <img class="card-img-top" 
				 src="<?php echo get_field("slick")[0]["image"]["url"]; ?>" 
				 alt="<?php echo get_field("slick")[0]["image"]["alt"]; ?>">
        </div>
      </a>
      <div class="card-body">
        <h5 class="card-title"><?php echo wp_trim_words(get_the_title(), 20, '...'); ?></h5>
        <div class="card-info">
		  <span><?php echo get_field("content2"); ?>,<?php echo get_field("content3"); ?>坪</span>
		  <span><?php echo get_the_category()[0]->name; ?></span>
        </div>
      </div>
	</div>

	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	




	
  </div>
  <!--線上諮詢 -->
  <?php get_template_part("template-parts/content", "online-consult"); ?> 
</section>



<?php endwhile; ?>
<?php endif; ?>



	

<?php
//get_sidebar();
get_footer();
