<section class="container-fluid contact-info-section"  >
  <div class="container contact-info">
    <img   src="./src/dist/images/logo-footer.svg"  class="contact-logo" alt="logo">
    <p itemprop="addressLocality">台北聯絡處 | 台北市中山區雙城街49巷6號1F</p>
    <p itemprop="addressLocality">新北聯絡處 | 新北市萬里區瑪鍊頂街47之2號</p>
    <p>
      <a href="#" class="social-icon"><img src="./src/dist/images/youtube.svg" alt="youtube"></a>
      <a href="https://www.facebook.com/YUNCHUANGDESIGN01/" class="social-icon"><img src="./src/dist/images/facebook.svg" width="20px" alt="facebook"><span>follow us on Facebook</span></a> 
    </p>
    <div class="qrcode-line">
      <p class="mb-0">歡迎諮詢</p>
      <img src="./src/dist/images/QRcode-test.png" alt="line-QRcode">
    </div> 
    <div class="gotop">
        ↾
    </div>
  </div>
</section>
<footer class="container-fluid"> 
  <p itemprop="startDate" content="2015-09-07">©2018 Moveon-design版權所有 <span>All right reserved</span></p>
</footer>
