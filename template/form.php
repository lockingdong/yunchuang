<section class="container-fluid contact-us">
  <div class="container contact-wrapper">
    <div class="contact-us-title">
      <h2 class="mb-0">聯絡我們</h2>
      <h2 class="mb-0">CONTACT US</h2>
    </div>
    <div class="row contact-row">
      <form class="col m12">
        <div class="row">
          <div class="input-field col col-md-6 col-12 ">
            <input id="name" type="text" class="validate" name="name" required>
            <label for="name">聯絡人 *</label>
          </div>
          <div class="input-field col col-md-6 col-12 ">
            <select>
              <option value="" disabled selected>性別</option>
              <option value="men">男</option>
              <option value="male">女</option>
              <option value="N/A">N/A</option>
            </select>
          </div>
          <div class="input-field col col-md-6 col-12 ">
            <input id="tel" type="tel"  class="validate"  pattern="[0-9]{8,12}" title="請輸入聯絡電話(8~12碼)" name="tel" required >
            <label for="tel">聯絡電話 *</label>
          </div>
          <div class="input-field col col-md-6 col-12 ">
            <input id="email" type="email" class="validate" name="email" >
            <label for="email">信箱</label>
          </div>        
          <div class="input-field col col-md-6 col-12 ">
            <input id="address" type="text" class="validate" name="address" required>
            <label for="address">地址 *</label>
          </div>        
          <div class="input-field col col-md-6 col-12 ">
            <select>
              <option value="" disabled selected>空間類別</option>
              <option value="住宅">住宅</option>
              <option value="商業空間">商業空間</option>
              <option value="辦公室">辦公室</option>
            </select>
          </div>       
          <div class="input-field col col-md-6 col-12 ">
            <input id="member" type="text" class="validate" name="member" >
            <label for="member">家庭成員</label>
          </div>       
          <div class="input-field col col-md-6 col-12 ">
            <select>
              <option value="" disabled selected>自訂預算</option>
              <option value="100~150萬">100~150萬</option>
              <option value="150~200萬">150~200萬</option>
              <option value="200~300萬">200~300萬</option>
              <option value="300~500萬">300~500萬</option>
              <option value="500萬以上">500萬以上</option>
            </select>
          </div>       
          <div class="input-field col col-md-3 col-12 ">
            <select>
              <option value="" disabled selected>坪數</option>
              <option value="30坪以下">30坪以下</option>
              <option value="30~40坪">30~40坪</option>
              <option value="40~50坪">40~50坪</option>
              <option value="50~100坪">50~100坪</option>
              <option value="100坪以上">100坪以上</option>
            </select>
          </div>       
          <div class="input-field col col-md-3 col-12 ">
            <select>
              <option value="" disabled selected>房數</option>
              <option value="1房">1房</option>
              <option value="2房">2房</option>
              <option value="3房">3房</option>
              <option value="4房">4房</option>
              <option value="5房">5房</option>
              <option value="5房以上">5房以上</option>
            </select>
          </div>       
          <div class="input-field col col-md-3 col-12 ">
            <select>
              <option value="" disabled selected>廳數</option>
              <option value="1廳">1廳</option>
              <option value="2廳">2廳</option>
              <option value="3廳">3廳</option>
              <option value="4廳">4廳</option>
              <option value="5廳">5廳</option>
              <option value="5廳以上">5廳以上</option>
           
            </select>
          </div>       
          <div class="input-field col col-md-3 col-12 ">
            <select>
              <option value="" disabled selected>衛數</option>
              <option value="1衛">1衛</option>
              <option value="2衛">2衛</option>
              <option value="3衛">3衛</option>
              <option value="4衛">4衛</option>
              <option value="5衛">5衛</option>
              <option value="5衛以上">5衛以上</option>
            </select>
          </div>
          <div class="col col-12">
            <textarea name="remarks" id="" placeholder="請留下您的訊息"></textarea>
          </div>
          <div class="col col-12">
            <button class="btn btn-submit" type="submit">送出</button>
            <button class="btn btn-clear" type="reset">清空</button>
          </div>       
        </div>
      </form>
      
    </div>
    <div class="row">
      <div class="col-12 form-QRcode">
        <a href="line://ti/p/@mxd3546t">
          <img src="./src/dist/images/line-at.svg" alt="line-QRcode">
        </a>
        <div>
          <p class="mb-1">歡迎加入</p>
          <p class="mb-0">云創設計-Line家族，為您量身定制幸福居家</p>
        </div>
      </div>
    </div>
   
  </div>
  
</section>