<header class="container-fluid navbar navbar-bg" >
  <nav class="navbar  navbar-expand-lg navbar-light ">
    <a class="navbar-brand" href="index.php" >
      <img src="./src/dist/images/logo-top.svg" alt="YUNCHUANG logo">
    </a>
    <button class="navbar-toggler" type="button" id="dropdownbutton">
      <div class="hamburger" id="hamburger-9">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
      </div>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" itemprop="startDate" content="2015-09-07">
            <p class="nav-link-tw">公司介紹</p>
            <p class="nav-link-en">ABOUT US</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item dropdowm-link-a" data-target="about-area" href="about-us.php?#about" >關於云創</a>
            <a class="dropdown-item dropdowm-link-a" data-target="workflow-area" href="about-us.php?#workflow">作業流程</a>
            <a class="dropdown-item dropdowm-link-a" data-target="charge-area" href="about-us.php?#charge-reference" >收費參考</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">服務項目</p>
            <p class="nav-link-en">SERVICES</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="services.php">室內設計</a>
            <a class="dropdown-item" href="services.php">裝潢工程</a>
            <a class="dropdown-item" href="services.php">3D 設計</a>
            <a class="dropdown-item" href="services.php">系統工程</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">作品欣賞</p>
            <p class="nav-link-en">PROJECTS</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="project-list.php">住宅空間</a>
            <a class="dropdown-item" href="project-list.php">商業空間</a>
            <a class="dropdown-item" href="project-list.php">3D 作品</a>
            <a class="dropdown-item" href="project-list.php">系統案例</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <p class="nav-link-tw">影音專區</p>
            <p class="nav-link-en">MEDIA</p>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="video-list.php">工程專輯</a>
            <a class="dropdown-item" href="video-list.php">3D 環景</a>
            <a class="dropdown-item" href="video-list.php">影音教學</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contact.php" >
            <p class="nav-link-tw">聯絡我們</p>
            <p class="nav-link-en">CONTACT US</p>
          </a>
        </li>
      </ul>
    </div>
  </nav>
</header>