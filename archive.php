<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ken-cens.com
 */


 
$al_cat_slug= get_queried_object()->slug;
$al_cat_name= get_queried_object()->name;
$middle_pages = json_decode('[
	{
		"name": "作品欣賞",
		"url": "'. get_the_permalink(148) .'"
	}
]');
$page_title = $al_cat_name;
$custom_page_title = get_field('page_title', 148);
$cover_bg = get_field('cover_img', 148)['url'];
get_header(); ?>
<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>





<section class="container content-wrapper ">
  <div class="items-tab">
    <!-- <a href="#" class="btn btn-tab btn-active">住宅空間</a>
    <a hred="#" class="btn btn-tab">商業空間</a>
    <a hred="#" class="btn btn-tab">3D作品</a>
	<a hred="#" class="btn btn-tab">系統案例</a> -->
	<?php 

	$categories = get_categories();
		foreach($categories as $category) {
		//echo $category->slug;
		echo '<a class="btn btn-tab '. (($category->slug==$al_cat_slug)?"btn-active":"") .'" href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
	}

	?>
  </div>

<?php
	$cur = get_query_var('paged');
	$the_query = new WP_Query(array(
		
		'posts_per_page'=> 9,
		'post_type' => 'post',
		'paged' => $cur,
		//'ignore_sticky_posts' => 1,
		'meta_key'			=> 'test',
		'orderby'			=> 'meta_value date',
		'tax_query' => array( 
			array( 
				//'post__in' => get_option( 'sticky_posts' ),
				'taxonomy' => 'category', 
				'field' => 'slug', 
				'terms' => $al_cat_slug
			) 
		) 
	));


	//$array = $the_query->posts;


	//array_unshift($the_query->posts, []);
	//echo count($array);
?>

<div class="row project-list">

<?php while ($the_query -> have_posts()) : 
	$the_query -> the_post(); 
?>

    <div class="project-list-item col-lg-4 col-sm-6">
			<?php //echo "<pre>",var_dump(get_field("test")),"</pre>"; ?>
      <div class="card-item">
        <a href="<?php echo get_permalink() ;?>">
          <div class="card-img">
			<img class="card-img-top" 
				 src="<?php echo get_field("slick")[0]["image"]["url"]; ?>" 
				 alt="<?php echo get_field("slick")[0]["image"]["alt"]; ?>">
          </div>
        </a>
        <div class="card-body">
          <h5 class="card-title"><?php echo wp_trim_words( get_the_title(), 60, '...' ); ?></h5>
          <div class="card-info">
			<span>
				<?php echo get_field("content2"); ?>,<?php echo get_field("content3"); ?>坪
			</span>
			<span>
				<?php $cates = get_the_category(); ?>
				<?php foreach($cates as $cate): ?>
					
					<?php 
						echo	$cate->name;
					?>
				<?php endforeach; ?>
			
			</span>
          </div>
        </div>
      </div>  
	</div>
	

<?php
	endwhile;
	wp_reset_postdata(); 
?>
	
	


  </div>
  <!-- 分頁 -->

  <nav class="nav-pagination" aria-label="Page navigation">
	<?php my_pagination(); ?>
  </nav>


  <?php get_template_part("template-parts/content", "online-consult"); ?> 
</section>










<?php
get_footer();
