<?php
/**
  Template Name: 服務項目
 */

$middle_pages = json_decode('[
	{
		"name": "服務項目",
		"url": "#"
	}
]');
$page_title = get_the_title();
$custom_page_title = get_field('page_title');
$cover_bg = get_field('about_bg')['url'];
get_header(); ?>


<style>
  .services-board h2 {
    font-family: "Frank Ruhl Libre" ,"微軟正黑體" !important;
  }
</style>




<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>




<section class="container-fluid content-wrapper services-info">
  <div class="row">
    <div class="col-lg-6 services-info-col">
      <article class="services-info-wrapper">
        <div class="services-info-content"> 
          <h4><?php echo get_field('s1_title'); ?></h4>
          <div>
		  	<?php echo get_field('s1_content'); ?>
          </div>
        </div>
      </article>
    </div>
    <div class="col-lg-6 services-pic">
		<img src="<?php echo get_field('s1_img')['url']; ?>" 
			alt="<?php echo get_field('s1_img')['alt']; ?>"
		>
    </div>
  </div>
</section>






<section class="jumbotron jumbotron-fluid jumbotron-services-board"
		 style="background-image:url(<?php echo get_field('s2_img')['url']; ?>);">
  <div class="container services-board">
    <h2 ><?php echo get_field('s2_title'); ?></h2>
    <p>
		<?php echo get_field('s2_content'); ?>
    </p>
  </div>
</section>




<section class="jumbotron jumbotron-fluid jumbotron-services-board-house"
		 style="background-image:url(<?php echo get_field('s3_img')['url']; ?>)"
>
  <div class="services-info-content"> 
    <h4><?php echo get_field('s3_title'); ?></h4>
    <div>
		<?php echo get_field('s3_content'); ?>
    </div>
  </div>
</section>
	

<?php
get_footer();
