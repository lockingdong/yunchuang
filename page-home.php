<?php
/**
  Template Name: 首頁
 */


 
get_header(); ?>

<style>
</style>



<section class="header-block"></section>
<section class="container-fluid header-slider" style="cursor:pointer;">
<?php if( have_rows('banner') ): ?>
    <?php while( have_rows('banner') ): the_row(); ?>


	<div class="slider-item" 
		 style="background-image:url(<?php echo get_sub_field('bg_img')['url']; ?>);cursor:pointer;"
	>
		<div class="header-slogan" style="cursor:pointer;">
			<h2><?php echo get_sub_field('first_text'); ?></h2>
			<span><?php echo get_sub_field('second_text'); ?></span>
		</div>
			<a target="_blank" href="<?php echo get_sub_field('url'); ?>"></a>
	</div>

	<?php endwhile; ?>
<?php endif; ?>
 
</section>

<?php get_template_part("template-parts/content", "form"); ?>


<script>
	
	





// $(".blur-img").height($(".contact-us").outerHeight())  


// function getBackgroundSize(elem) {
//     var computedStyle = getComputedStyle(elem),
//         image = new Image(),
//         src = computedStyle.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2'),
//         cssSize = computedStyle.backgroundSize,
//         elemW = parseInt(computedStyle.width.replace('px', ''), 10),
//         elemH = parseInt(computedStyle.height.replace('px', ''), 10),
//         elemDim = [elemW, elemH],
//         computedDim = [],
//         ratio;
//     image.src = src;
//     ratio = image.width > image.height ? image.width / image.height : image.height / image.width;
//     cssSize = cssSize.split(' ');
//     computedDim[0] = cssSize[0];
//     computedDim[1] = cssSize.length > 1 ? cssSize[1] : 'auto';
//     if(cssSize[0] === 'cover') {
//         if(elemDim[0] > elemDim[1]) {
//             if(elemDim[0] / elemDim[1] >= ratio) {
//                 computedDim[0] = elemDim[0];
//                 computedDim[1] = 'auto';
//             } else {
//                 computedDim[0] = 'auto';
//                 computedDim[1] = elemDim[1];
//             }
//         } else {
//             computedDim[0] = 'auto';
//             computedDim[1] = elemDim[1];
//         }
//     } else if(cssSize[0] === 'contain') {
//         if(elemDim[0] < elemDim[1]) {
//             computedDim[0] = elemDim[0];
//             computedDim[1] = 'auto';
//         } else {
//             if(elemDim[0] / elemDim[1] >= ratio) {
//                 computedDim[0] = 'auto';
//                 computedDim[1] = elemDim[1];
//             } else {
//                 computedDim[1] = 'auto';
//                 computedDim[0] = elemDim[0];
//             }
//         }
//     } else {
//         for(var i = cssSize.length; i--;) {
//             if (cssSize[i].indexOf('px') > -1) {
//                 computedDim[i] = cssSize[i].replace('px', '');
//             } else if (cssSize[i].indexOf('%') > -1) {
//                 computedDim[i] = elemDim[i] * (cssSize[i].replace('%', '') / 100);
//             }
//         }
//     }
//     if (computedDim[0] === 'auto' && computedDim[1] === 'auto') {
//         computedDim[0] = image.width;
//         computedDim[1] = image.height;
//     } else {
//         ratio = computedDim[0] === 'auto' ? image.height / computedDim[1] : image.width / computedDim[0];
//         computedDim[0] = computedDim[0] === 'auto' ? image.width / ratio : computedDim[0];
//         computedDim[1] = computedDim[1] === 'auto' ? image.height / ratio : computedDim[1];
//     }
//     return {
//         width: computedDim[0],
//         height: computedDim[1]
//     };
// }
// function updateData(){
//   var el = document.getElementById("wrap");
//   var bgs = getBackgroundSize(el);
//   $(".blur-img").width(bgs.width)
//   $(".blur-img").height(bgs.height)  
// }


// window.onresize = updateData;
// updateData();


</script>
<?php
get_footer();
