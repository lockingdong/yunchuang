<section class="jumbotron jumbotron-fluid jumbotron-online-consult">
  <div class="container online-consult">
    <div class="online-consult-info">
      <h5>空間規劃諮詢</h5>
      <p>等問題，請填寫表單或直接加入Line社群</p>
    </div>
    <div>
      <a href="<?php echo get_the_permalink(172); ?>" class="btn ">線上諮詢</a>
      <a href="<?php echo get_field('line' ,179); ?>" >
        <img src="<?php bloginfo("stylesheet_directory");?>/src/dist/images/line-addfriend.svg" alt="line-qrcode">
      </a>
    </div>
  </div>
</section>