<section class="jumbotron jumbotron-fluid jumbotron-breadcrumb jumbotron-contact"
		 style="background-image:url(<?php echo $cover_bg;?>)"
>
  <div class="container">
    <h2 class="mb-0" ><?php echo $custom_page_title;?></h2>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>">首頁</a></li>

		<?php if(count($middle_pages)>0): ?>
			<?php foreach($middle_pages as $key => $page): ?>
				<li class="breadcrumb-item">
					<a href="<?php echo $page->{'url'}; ?>">
						<?php echo $page->{'name'}; ?>
					</a>
				</li>
			<?php endforeach;?>
		<?php endif; ?>

        <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title; ?></li>
      </ol>
    </nav>
  </div>
</section>
