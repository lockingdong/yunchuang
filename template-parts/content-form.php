<section class="container-fluid contact-us" id="wrap"
		 style="background-image:url(<?php echo get_field('contact_bg', 179)['url']; ?>)"		
>
  <div class="container contact-wrapper">
    <div class="contact-us-title">
      <h2 class="mb-0">聯絡我們</h2>
      <h2 class="mb-0">CONTACT US</h2>
    </div>
    <div class="row contact-row">
		<div class="blur-img"
			 style="background-image:url(<?php echo get_field('contact_bg', 179)['url']; ?>)"
		></div>
		<?php echo do_shortcode( '[contact-form-7 id="5" title="聯絡我們"]' );?>
	</div>
    <div class="row">
      <div class="col-12 form-QRcode">
        <a href="<?php echo get_field('line', 179); ?>">
          <img src="<?php echo bloginfo('stylesheet_directory'); ?>/src/dist/images/line-at.svg" alt="line-QRcode">
        </a>
        <div>
          <p class="mb-1"><?php echo get_field('line_p', 179); ?></p>
          <p class="mb-0"><?php echo get_field('line_p2', 179); ?></p>
        </div>
      </div>
    </div>
   
  </div>
  
</section>