// 首頁
$('.header-slider').slick({
  dots: true,
  arrows: false,
  swipeToSlide: true,
  customPaging: function customPaging(slider, i) {
    return '<a href="javascript:;" class="slider-dots"></a>';
  }
});
// 收費參考
$(".charge-slick").slick({
  dots: false,
  arrows: true,
  swipeToSlide: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  prevArrow: '<button type="button" class="slick-prev">↼</button>',
  nextArrow: '<button type="button" class="slick-next">⇀</button>',
  responsive: [{
    breakpoint: 992,
    settings: {
      slidesToShow: 2
    }
  }, {
    breakpoint: 768,
    settings: {
      slidesToShow: 1
    }
  }]
});
// 作品內頁 slick
$('.project-main-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.project-sub-slider'
});
$('.project-sub-slider').slick({
  slidesToShow: 8,
  slidesToScroll: 1,
  asNavFor: '.project-main-slider',
  dots: false,
  swipeToSlide: true,
  // centerMode: true,
  focusOnSelect: true,
  prevArrow: '<button type="button" class="slick-prev">↽</button>',
  nextArrow: '<button type="button" class="slick-next">⇁</button>',
  responsive: [{
    breakpoint: 992,
    settings: {
      slidesToShow: 5
    }
  }, {
    breakpoint: 768,
    settings: {
      slidesToShow: 4
    }
  }, {
    breakpoint: 576,
    settings: {
      slidesToShow: 3
    }
  }]
});
// 作品內頁 精選介紹
$('.project-introduction-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: true,
  arrows: false,
  swipeToSlide: true,
  focusOnSelect: true,
  customPaging: function customPaging(slider, i) {
    return '<a href="javascript:;" class="slider-dots"></a>';
  },
  responsive: [{
    breakpoint: 992,
    settings: {
      slidesToShow: 2
    }
  }, {
    breakpoint: 768,
    settings: {
      slidesToShow: 1
    }
  }]
});
$(document).ready(function () {
  //關於我們 網址判斷滑到該區域
  var loc_href = window.location.href;
  var s = loc_href.substring(0, loc_href.indexOf("?#"));
  setTimeout(function () {
    if (loc_href.indexOf("about-us.php?#about") > -1) {
      $("body, html").animate({
        scrollTop: $(".about-area").offset().top - 120
      }, 1000);
      window.history.pushState("", "", s);
    } else if (loc_href.indexOf("about-us.php?#workflow") > -1) {
      $("body, html").animate({
        scrollTop: $(".workflow-area").offset().top - 120
      }, 1000);
      window.history.pushState("", "", s);
    } else if (loc_href.indexOf("about-us.php?#charge-reference") > -1) {
      $("body, html").animate({
        scrollTop: $(".charge-area").offset().top - 120
      }, 1000);
      window.history.pushState("", "", s);
    }
  }, 500);
  //關於我們 當前頁面選選單
  $(".dropdowm-link-a").on("click", function () {
    if (loc_href.indexOf("about-us.php?") > -1) {
      var _target = $(this).data("target");
      var _offset = $("." + _target).offset().top;
      $("body, html").animate({
        scrollTop: _offset - 120
      }, 1000);
    }
  });
  //表單
  $('select').formSelect();
  //navbar 992以上click沒反應 
  // $(window).on("resize",()=>{
  if ($(window).width() > 992) {
    $(".dropdown").on("click", function (e) {
      e.stopPropagation();
      return ' ';
    });
  };
  // })

  // 漢堡按鈕
  function checkClass(el, toggle) {
    $(el).hasClass(toggle) ? $(el).removeClass(toggle) : $(el).addClass(toggle);
  }
  $(".hamburger").on("click", function () {
    checkClass(".hamburger", "is-active");
  });
  //首頁漢堡按鈕
  $('#dropdownbutton').on("click", function () {
    checkClass(".navbar-toggler", "open");
    checkClass(".navbar-collapse", "open");
  });

  //影片集結頁 字數限制
  function substringfunc($len, selector) {
    $(selector).each(function () {
      if ($(this).text().length > $len) {
        var $text = $(this).text().substring(0, $len - 1) + "...";
        $(this).text($text);
      }
    });
  }
  substringfunc(30, ".video-list .card-item .card-info span");

  $(window).on("scroll", function () {
    $(window).scrollTop() > 200 ? $(".gotop").css("opacity", '.7') : $(".gotop").css("opacity", '0');
  });
  $(".gotop").on('click', function () {
    $("html,body").animate({
      scrollTop: 0
    }, 700);
    return false;
  });
  
  var fbButton = document.querySelector('#fb-share-button');
  var url = window.location.href;

  fbButton.addEventListener('click', function () {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, 'facebook-share-dialog', 'width=800,height=600');
    return false;
  });
});