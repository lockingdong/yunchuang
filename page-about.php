<?php
/**
  Template Name: 公司介紹
 */

$middle_pages = json_decode('[
]');
$page_title = get_the_title();
$custom_page_title = get_field('page_title');
$cover_bg = get_field('about_bg')['url'];
get_header(); ?>



<?php $member_count = count(get_field('workflows')); ?>
<?php 
	echo "<style>"
?>
<?php if($member_count):$i=1;?>
	<?php while($i<=$member_count):?>
	.workflow-step<?php echo $i ;?>::before {
		content: "<?php echo $i ?>";;
		color: <?php echo ($i==$member_count)?'#fcc802 !important':'';?>;
	}
	<?php $i++;?>
	<?php endwhile; ?>
<?php endif;?>
<?php 
	echo "</style>"
?>
<style>
.workflow-step {
	position: relative;
}

.workflow-step::before {
    position: absolute;
    display: inline-block;
    color: #e9e9e9;
    font-family: Anton,sans-serif;
    font-size: 68px;
    top: -120%;
    z-index: -1;
}
.profile-pic img{
	max-width: 300px
}
.charge-reference .charge-slick .slick-next{
	right: -16%!important;
}
.charge-reference .charge-slick .slick-prev{
	left: -16%!important;
}
</style>


<?php set_query_var( 'middle_pages', $middle_pages ); ?>
<?php set_query_var( 'page_title', $page_title ); ?>
<?php set_query_var( 'custom_page_title', $custom_page_title ); ?>
<?php set_query_var( 'cover_bg', $cover_bg ); ?>
<?php get_template_part("template-parts/content", "page-header"); ?>


<section class="container-fluid content-wrapper about-us " >
  <div class="container about-area">



<?php if( have_rows('members') ): ?>
    <?php while( have_rows('members') ): the_row(); ?>

    <div class="row profile">
      <div class="col-md-6 page-title">
        <h1 class="before-line">關於<span>云創設計</span></h1>
        <p><?php echo get_sub_field('full_name'); ?></p>
        <br>
        <div class="profile-license">
		<?php if( have_rows('any_infos') ): ?>
			<?php while( have_rows('any_infos') ): the_row(); ?>
		  		<p>領有 <?php echo get_sub_field('info'); ?></p>
			<?php endwhile; ?>
		<?php endif; ?>
          
        </div>
        <br>
        <p><?php echo get_sub_field('content'); ?></p>
      </div> 
      <div class="col-md-6 profile-pic">
        <img src="<?php echo get_sub_field('pic')['url']; ?>" alt="<?php echo get_sub_field('pic')['alt']; ?>" >
      </div>
	</div>
		


    <?php endwhile; ?>
<?php endif; ?>





  </div>
  <div class="container-fluid  content-wrapper-s workflow">
    <div class="container workflow-section workflow-area">
        <h2 class="section-title after-line">作業<span>流程</span></h2>
        <div class="row">





          
		<?php if( have_rows('workflows') ): $i=1;?>
			<?php while( have_rows('workflows') ): the_row(); ?>


				<div class="col-md-4 col-sm-6  workflow-step">
					<h3 class="workflow-step workflow-step<?php echo $i++ ;?>"><?php echo get_sub_field('title'); ?></h3>
					<?php echo get_sub_field('content'); ?>
				</div>
				
			<?php endwhile; ?>
		<?php endif; ?>







          
        </div>
      </div>
      <img class="workflow-bg" src="<?php echo bloginfo('stylesheet_directory'); ?>/src//dist/images/workflow-bg.png" alt="">
  </div>
  <div class="container content-wrapper-s charge-area" >
    <h2 class="section-title before-line">收費<span>參考</span></h2>
	<div class="charge-reference"
		 style="background-image:url(<?php echo get_field('bg_img')['url']; ?>)"
	>
      <h4><?php echo get_field('bg_text'); ?></h4>
	  <div class="charge-mark"></div>
	  <div class="charge-slick">




<?php if( have_rows('slick') ): ?>
    <?php while( have_rows('slick') ): the_row(); ?>

		<?php if(!get_sub_field('custom_price')): ?>
		<div class="card" 
			 style="background:url(<?php echo get_sub_field('bg')['url']; ?>);"
		>
          <div class="card-body">
            <h5 class="card-title">
				<img src="<?php echo get_sub_field('title_icon')['url']; ?>" 
					 alt="<?php echo get_sub_field('title_icon')['alt']; ?>">
				<?php echo get_sub_field('title'); ?>
			</h5>
            <p class="card-text mb-0"><?php echo get_sub_field('sub_title'); ?></p>
            <p class="card-price"><sup>NT:</sup><?php echo get_sub_field('low_price'); ?></p>
            <p class="card-price">~</p>
            <p class="card-price card-price-high"><sup>NT:</sup><?php echo get_sub_field('high_price'); ?></p>
          </div>
		</div>

		<?php else: ?>
		<div class="card" 
			 style="background:url(<?php echo get_sub_field('custom_bg')['url']; ?>);"
		>
          <div class="card-body">
			<h5 class="card-title">
				<img src="<?php echo get_sub_field('custom_icon')['url']; ?>" 
					 alt="<?php echo get_sub_field('custom_icon')['alt']; ?>">
				<?php echo get_sub_field('custom_title'); ?>
			</h5>
            <p class="card-text mb-0">每坪</p>
            <br>
            <br>
            <p class="card-price card-price-high"><?php echo get_sub_field('custom_text'); ?></p>
          </div>
		</div>
		<?php endif; ?>
		
	<?php endwhile; ?>
<?php endif; ?>
        
        
    
      </div>
    </div>
    </div>
  </div>
</section>




<script>

var loc_href = window.location.href;
var s = loc_href.substring(0, loc_href.indexOf("?#"));
setTimeout(function () {
if (loc_href.indexOf("#about") > -1) {
	$("body, html").animate({
	scrollTop: $(".about-area").offset().top - 120
	}, 1000);
	window.history.pushState("", "", s);
} else if (loc_href.indexOf("#workflow") > -1) {
	$("body, html").animate({
	scrollTop: $(".workflow-area").offset().top - 120
	}, 1000);
	window.history.pushState("", "", s);
} else if (loc_href.indexOf("#charge-reference") > -1) {
	$("body, html").animate({
	scrollTop: $(".charge-area").offset().top - 120
	}, 1000);
	window.history.pushState("", "", s);
}
}, 500);


</script>




	

<?php
get_footer();
